﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "KandooZ/RoomData")]
public class RoomsData : ScriptableObject
{

    [SerializeField] private Room[] noExitRoomsPrefabs;
    [SerializeField] private Room[] oneExitRoomsPrefabs;
    [SerializeField] private Room[] twoExitRoomsPrefabs;
    [SerializeField] private Room[] threeExitRoomsPrefabs;
    [SerializeField] private Room[] shopRoomsPrefabs;
    [SerializeField] private Room[] itemRoomsPrefabs;
    [SerializeField] private Room[] bossRoomsPrefabs;

    public Room GetRoom(RoomType roomType)
    {
        switch (roomType)
        {
            case RoomType.NoExit:
                return noExitRoomsPrefabs.GetRandomValue();
            case RoomType.OneExit:
                return oneExitRoomsPrefabs.GetRandomValue();
            case RoomType.TwoExit:
                return twoExitRoomsPrefabs.GetRandomValue();
            case RoomType.ThreeExit:
                return threeExitRoomsPrefabs.GetRandomValue();
            case RoomType.Shop:
                return shopRoomsPrefabs.GetRandomValue();
            case RoomType.Item:
                return itemRoomsPrefabs.GetRandomValue();
            case RoomType.Boss:
                return bossRoomsPrefabs.GetRandomValue();
        }

        return null;
    }

}
