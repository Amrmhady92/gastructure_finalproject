﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraTargetFollower : MonoBehaviour
{
    //public float worldLimitsLeft = -5;
    //public float worldLimitsRight = -5;
    public float maxDistance = 2;
    public float minDistance = 1;
    public float height = 2;
    public float lerpSpeed = 5;

    public CharacterController2D playerObject;
    //private void Update()
    //{
    //    //if (this.transform.position.x >= worldLimitsLeft && this.transform.position.x <= worldLimitsRight)
    //    //{
    //        if (Mathf.Abs(this.transform.position.x - playerObject.transform.position.x) > maxDistance)
    //        {
    //            if (this.transform.position.x > playerObject.transform.position.x)
    //            {
    //            //this.transform.position = playerObject.transform.position + new Vector3(maxDistance, 0, 0);
    //            this.transform.position = new Vector3(playerObject.transform.position.x - maxDistance, this.transform.position.y, this.transform.position.z);

    //            }
    //            else
    //            {
    //            this.transform.position = new Vector3(playerObject.transform.position.x + maxDistance, this.transform.position.y, this.transform.position.z);
    //            }
    //        }

    //    if (Mathf.Abs(this.transform.position.y - playerObject.transform.position.y) > height)
    //    {
    //        if (this.transform.position.y > playerObject.transform.position.y)
    //        {
    //            //this.transform.position = playerObject.transform.position + new Vector3(maxDistance, 0, 0);
    //            this.transform.position = new Vector3(this.transform.position.x, playerObject.transform.position.y - height, this.transform.position.z);

    //        }
    //        else
    //        {
    //            this.transform.position = new Vector3(this.transform.position.x, playerObject.transform.position.y + height, this.transform.position.z);
    //        }
    //    }

    //    //this.transform.position = new Vector3(this.transform.position.x, playerObject.transform.position.y + height, this.transform.position.z);
    //    //}
    //    //if (this.transform.position.x < worldLimitsLeft) this.transform.position = new Vector3(worldLimitsLeft, this.transform.position.y, this.transform.position.z);
    //    //if (this.transform.position.x > worldLimitsRight) this.transform.position = new Vector3(worldLimitsRight, this.transform.position.y, this.transform.position.z);
    //}

    private float awayDistanceX;
    private float awayDistanceY;
    private Vector3 target;
    private void FixedUpdate()
    {
        target = playerObject.transform.position;
        if (playerObject.Direction >= 0)
        {
            awayDistanceX = Mathf.Max(Mathf.Abs(this.transform.position.x - target.x), minDistance);
            awayDistanceX = Mathf.Min(maxDistance, awayDistanceX);
        }
        else
        {
            awayDistanceX = Mathf.Min(-Mathf.Abs(this.transform.position.x - target.x),-minDistance);
            awayDistanceX = Mathf.Max(-maxDistance, awayDistanceX);
        }


        target.x += awayDistanceX;
        target.y += height;
        target.z = this.transform.position.z;
        this.transform.position = Vector3.Lerp(this.transform.position, target, lerpSpeed * Time.deltaTime);
        
    }
}