﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterActions : MonoBehaviour
{

    Interactable currentInteractable;
    private static CharacterActions instance;

    Collider2D[] hits;
    Interactable interactableHit;
    Interactable nearestInteractableHit;
    BoxCollider2D boxCollider;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (CurrentInteractable != null)
            {
                CurrentInteractable.Interact(this.gameObject);
            }
        }

        //hits = Physics2D.BoxCastAll(this.transform.position, this.GetComponent<BoxCollider2D>().size, 0,new Vector2(0.1f, 0));
        hits = Physics2D.OverlapBoxAll(transform.position, /*transform.localScale*/boxCollider.size, 0);
        nearestInteractableHit = null;
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                interactableHit = hits[i].GetComponent<Interactable>();
                if (interactableHit != null && interactableHit.interactable)
                {
                    if(nearestInteractableHit != null)
                    {
                        float nearest = Vector2.Distance(nearestInteractableHit.transform.position, this.transform.position);
                        if (nearest > Vector2.Distance(interactableHit.transform.position, this.transform.position))
                        {
                            nearestInteractableHit = interactableHit;
                        }
                    }
                    else
                    {
                        nearestInteractableHit = interactableHit;
                    }
                    
                }
            }
            CurrentInteractable = nearestInteractableHit;
        }
    }


    public Interactable CurrentInteractable
    {
        get
        {
            return currentInteractable;
        }

        set
        {
            if(currentInteractable != value)
            {
                if (currentInteractable != null)
                {
                    currentInteractable.SetInteractableImage(false);
                    currentInteractable = value;
                    if (currentInteractable != null) currentInteractable.SetInteractableImage(true);
                }
                else 
                {
                    if(value != null)
                    {
                        currentInteractable = value;
                        if (currentInteractable != null) currentInteractable.SetInteractableImage(true);
                    }
                }
               
            }

        }
    }


    private void Awake()
    {
        if (instance == null) instance = this;
        boxCollider = this.GetComponent<BoxCollider2D>();
    }
    public static CharacterActions Instance
    {
        get
        {
            return instance;
        }
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    Interactable interactable = collision.GetComponent<Interactable>();
    //    if (interactable != null && interactable.interactable)
    //    {
    //        //Debug.Log("Hit " + interactable.gameObject.name);
    //        CurrentInteractable = interactable;
    //    }
    //}

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    Interactable interactable = collision.GetComponent<Interactable>();
    //    if (interactable != null && interactable.interactable)
    //    {
    //        CurrentInteractable = null;
    //    }
    //}
}
