﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "KandooZ/ItemsList")]
public class ItemsList : ScriptableObject
{
    public List<Item> weaponsPrefabs;
    public List<Item> armorPrefabs;
    public List<Item> powersPrefabs;

    private List<Item> weaponsLeftList;
    private List<Item> armorLeftList;
    private List<Item> powersLeftList;
    public Item GetRandomItem(int tier, int specific = -1)
    {
        if (weaponsLeftList == null || weaponsLeftList.Count == 0) weaponsLeftList = new List<Item>(weaponsPrefabs);
        if (armorLeftList == null || armorLeftList.Count == 0) armorLeftList = new List<Item>(armorPrefabs);
        if (powersLeftList == null || powersLeftList.Count == 0) powersLeftList = new List<Item>(powersPrefabs);

        Item returnedItem = null;
        int randoming = 0;


        if (specific >= 0 && specific < 3)
        {
            randoming = specific;
        }
        else
        {
            randoming = tier <= 1 ? Random.Range(0, 2) : Random.Range(0, 3);
        }

        List<Item> itemsoftier = null;
        switch (randoming)
        {
            case 0:
                itemsoftier = weaponsLeftList.FindAll(x => x.tier == tier);
                returnedItem = itemsoftier.GetRandomValue();
                if (returnedItem == null)
                {
                    itemsoftier = weaponsPrefabs.FindAll(x => x.tier == tier);
                    returnedItem = itemsoftier.GetRandomValue();

                    if(returnedItem != null && itemsoftier.Count > 0)
                    {
                        for (int i = 0; i < itemsoftier.Count; i++)
                        {
                            if (returnedItem == itemsoftier[i]) continue;
                            weaponsLeftList.Add(itemsoftier[i]);
                        }
                    } 
                }
                if(returnedItem == null)
                {
                    Debug.Log("item Tier not found , returning anyitem");
                    returnedItem = weaponsLeftList.GetRandomValue();
                }
                if (returnedItem != null)
                {
                    weaponsLeftList.Remove(returnedItem);
                }
                else
                {
                    Debug.LogError("No items Found");
                }


                break;
            case 1:
                itemsoftier = armorLeftList.FindAll(x => x.tier == tier);
                returnedItem = itemsoftier.GetRandomValue();
                if (returnedItem == null)
                {
                    itemsoftier = armorPrefabs.FindAll(x => x.tier == tier);
                    returnedItem = itemsoftier.GetRandomValue();

                    if (returnedItem != null && itemsoftier.Count > 0)
                    {
                        for (int i = 0; i < itemsoftier.Count; i++)
                        {
                            if (returnedItem == itemsoftier[i]) continue;
                            armorLeftList.Add(itemsoftier[i]);
                        }
                    }
                }
                if (returnedItem == null)
                {
                    //Debug.Log("item Tier not found , returning anyitem");
                    returnedItem = armorLeftList.GetRandomValue();
                }
                if (returnedItem != null)
                {
                    armorLeftList.Remove(returnedItem);
                }
                else
                {
                    Debug.LogError("No items Found");
                }
                break;
            case 2:
                itemsoftier = powersLeftList.FindAll(x => x.tier == tier);
                returnedItem = itemsoftier.GetRandomValue();
                if (returnedItem == null)
                {
                    itemsoftier = powersPrefabs.FindAll(x => x.tier == tier);
                    returnedItem = itemsoftier.GetRandomValue();

                    if (returnedItem != null && itemsoftier.Count > 0)
                    {
                        for (int i = 0; i < itemsoftier.Count; i++)
                        {
                            if (returnedItem == itemsoftier[i]) continue;
                            powersLeftList.Add(itemsoftier[i]);
                        }
                    }
                }
                if (returnedItem == null)
                {
                    Debug.Log("item Tier not found , returning anyitem");
                    returnedItem = powersLeftList.GetRandomValue();
                }
                if (returnedItem != null)
                {
                    powersLeftList.Remove(returnedItem);
                }
                else
                {
                    Debug.LogError("No items Found");
                }
                break;
        }
        return returnedItem;
    }

 
}
