using UnityEngine;
using KandooZ;
using TMPro;

[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private bool active = true;
    [SerializeField] private bool running = false;
    [SerializeField] private int direction = 1;
    [SerializeField] private bool canDoubleJump = false;
    [SerializeField] private bool doubleJumpCharge = false;

    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    //float speed = 7;
    FloatField moveSpeed;

    [SerializeField, Tooltip("Acceleration while grounded.")]
    float walkAcceleration = 20;

    [SerializeField, Tooltip("Acceleration while in the air.")]
    float airAcceleration = 10;

    [SerializeField, Tooltip("Deceleration applied when character is grounded and not attempting to move.")]
    float groundDeceleration = 20;

    [SerializeField, Tooltip("Max height the character will jump regardless of gravity")]
    float jumpHeight = 2;

    private BoxCollider2D boxCollider;

    private Vector2 velocity;

    /// <summary>
    /// Set to true when the character intersects a collider beneath
    /// them in the previous frame.
    /// </summary>
    [SerializeField] private bool grounded;
    public TextMeshProUGUI groundedTextTest;
    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    public bool Running
    {
        get
        {
            return running;
        }
    }

    public bool Grounded
    {
        get
        {
            return grounded;
        }

        private set
        {
            grounded = value;
            if (groundedTextTest != null) groundedTextTest.text = grounded ? "True" : "False";
        }
    }

    public int Direction
    {
        get
        {
            return direction;
        }

        protected set
        {
            direction = value;
        }
    }

    public Vector2 Velocity
    {
        get
        {
            return velocity;
        }

        protected set
        {
            velocity = value;
        }
    }

    public FloatField MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            moveSpeed = value;
        }
    }

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }
    float moveInput;
    private void Update()
    {
        //if (!Active) return;
        // Use GetAxisRaw to ensure our input is either 0, 1 or -1.
       /* if (active)*/ moveInput = Input.GetAxisRaw("Horizontal");
        //else moveInput = 0;

        if (moveInput > 0)
        {
            this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);
            Direction = 1;
        }
        else if (moveInput < 0)
        {
            this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) * -1, this.transform.localScale.y, this.transform.localScale.z);
            Direction = -1;
        }


        if (!active) moveInput = 0;



        running = moveInput == 0 ? false : true;

        if (Grounded /*|| doubleJumpCharge*/)
        {
            velocity.y = 0;

            if ((Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.G)) && active)
            {
                // Calculate the velocity required to achieve the target jump height.
                velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));

            }
        }

        float acceleration = Grounded ? walkAcceleration : airAcceleration;
        float deceleration = Grounded ? groundDeceleration : 0;

        if (moveInput != 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, moveSpeed.Value * moveInput, acceleration * Time.deltaTime);
        }
        else
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
        }

        velocity.y += Physics2D.gravity.y * Time.deltaTime;

        transform.Translate(velocity * Time.deltaTime);

        Grounded = false;

        // Retrieve all colliders we have intersected after velocity has been applied.
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, /*transform.localScale*/boxCollider.size, 0);

        foreach (Collider2D hit in hits)
        {
            // Ignore our own collider.
            if (hit == boxCollider || hit.isTrigger)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

            // Ensure that we are still overlapping this collider.
            // The overlap may no longer exist due to another intersected collider
            // pushing us out of this one.
            if (colliderDistance.isOverlapped)
            {
                transform.Translate(colliderDistance.pointA - colliderDistance.pointB);

                // If we intersect an object beneath us, set grounded to true. 
                if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
                {
                    Grounded = true;
                    if(canDoubleJump) doubleJumpCharge = true;

                }
            }
        }
    }
}