﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using KandooZ;
using TMPro;

public class InteractableBuy : Interactable
{
    public int tier = 1;
    [Header("0 = Wpn, 1 = Armr , 2 = Pwr , -1 = Random")]
    public int itemSpecific = -1;
    public int itemPrice = 100;
    public Transform itemPosition;
    public Item itemObject;
    public ItemsList itemsList;
    public IntField playersGold;

    private TextMeshProUGUI itemPriceText;

    public UnityEvent onItemPurchased;
    public void Set(int tier, int itemSpecific)
    {
        this.tier = tier;
        this.itemSpecific = itemSpecific;

        if (itemPosition == null) itemPosition = this.transform;

        itemObject = itemsList.GetRandomItem(tier, itemSpecific);
        if(itemObject != null)
        {
            itemObject = GameObject.Instantiate(itemObject, itemPosition.parent);
            itemObject.GetComponent<ItemInteraction>().interactable = false;
            itemObject.transform.position = itemPosition.position;
            itemPrice = itemObject.itemPrice;
            itemPriceText = GetComponentInChildren<TextMeshProUGUI>();
            if (itemPriceText != null) itemPriceText.text = itemPrice.ToString();
            interactable = true;
        }
        else
        {
            interactable = false;
        }
    }
    public override void Interact(GameObject interactingObject = null)
    {
        if (!interactable || itemObject == null) return;

        if(playersGold.Value >= itemPrice)
        {
            interactable = false;
            ButtonClicked();
            onItemPurchased?.Invoke();

            if (itemPriceText != null) itemPriceText.gameObject.SetActive(false);

            itemObject.transform.LeanMove(itemObject.transform.position + new Vector3(0, -1, 0), 0.5f).setOnComplete(()=> 
            {
                itemObject.GetComponent<ItemInteraction>().interactable = true;
            });

            playersGold.Value -= itemPrice;

        }
        else
        {
            ShakeButton();
        }
    }
}
