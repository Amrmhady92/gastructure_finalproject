﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSwordNormal : Item
{
    public WeaponStats weaponStats = new WeaponStats();


    public override void OnItemDropped()
    {
        CharacterCombatController.Instance.SetWeaponStats(null);
    }

    public override void OnItemPick()
    {
        CharacterCombatController.Instance.SetWeaponStats(weaponStats);
    }
}
