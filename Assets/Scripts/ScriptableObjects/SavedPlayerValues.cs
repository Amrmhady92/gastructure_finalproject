﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "KandooZ/SavedPlayerValues")]
public class SavedPlayerValues : ScriptableObject
{
    public Item weapon;
    public Item armor;
    public Item power;

    public float currentHp;


}
