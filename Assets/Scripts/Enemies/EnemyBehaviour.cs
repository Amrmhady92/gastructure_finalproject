﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public enum EnemyDebuffs
{
    None,
    Stun,
    DoT,
    Charm,
    InstaKill,
}

public enum EnemyInflictedDebuffs
{
    Stun,
    DoT
}


public abstract class EnemyBehaviour : MonoBehaviour
{
    [SerializeField] private float hp = 4;
    public UnityEvent onHit;
    public UnityEvent onDeathBlow;
    public bool hitable = true;
    public EnemyDebuffs currentDebuff;
    protected int hitSide = 0;
    public float Hp
    {
        get
        {
            return hp;
        }

        set
        {
            if (hp <= 0) return;

            hp = value;
            if(hp <= 0)
            {
                hp = 0;
                Debug.Log("Dead");
                hitable = false;
                //gameObject.SetActive(false);
                onDeathBlow?.Invoke();
            }
            else
            {
                Debug.Log("hit");
                onHit?.Invoke();
            }
        }
    }

    public void HitRecieved(float damage, Vector3 hitSpot, EnemyDebuffs debuff)
    {
        if (!hitable) return;
        //Hit side
        hitSide = this.transform.position.x - hitSpot.x > 0 ? 1 : -1;
        Hp -= damage;
        if (Hp <= 0)
        {
            OnDeath();
        }
        else
        {
            ApplyDebuff(debuff);
            OnHit();
        }
        
    }

    protected abstract void OnHit();
    protected abstract void OnHitPlayer();
    protected abstract void OnDeath();

    protected abstract void ApplyDebuff(EnemyDebuffs debuff);
}
