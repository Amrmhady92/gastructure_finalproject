﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using KandooZ;
public class CharacterStats : MonoBehaviour
{

    [SerializeField] private FloatField hp;
    public FloatField attackDamage;
    public FloatField attackSpeed;
    public FloatField moveSpeed;
    public FloatField maxHp;
    public FloatField sheildBlockTicks;
    [Space(10)]
    public Transform itemDroppingTransform;
    [Space(10)]
    public DefaultValues defaultValues;
    [Space(10)]
    public IntField gold;
    [Space(10)]
    public Image weaponIconSpriteRenderer;
    public Image armorIconSpriteRenderer;
    public Image powerIconSpriteRenderer;

    public LayerMask groundLayers;
    public Transform itemEffectParent;

    private Sprite weaponSpriteIcondefault;
    private Sprite armorSpriteIcondefault;
    private Sprite powerSpriteIcondefault;

    [SerializeField] private Item weaponItem;
    [SerializeField] private Item armorItem;
    [SerializeField] private Item powerItem;

    
    // UI Manager
    private static CharacterStats instance;

    public static CharacterStats Instance
    {
        get
        {
            return instance;
        }

        protected set
        {
            instance = value;
        }
    }

    public Room currentRoom;

    private CharacterController2D controller;
    public Item WeaponItem
    {
        get
        {
            return weaponItem;
        }

        protected set
        {
            CheckAndRemoveOldItem(weaponItem, value, weaponIconSpriteRenderer, weaponSpriteIcondefault);
            weaponItem = value;
            SubmitNewItem(weaponItem, weaponIconSpriteRenderer);
        }
    }

    public Item ArmorItem
    {
        get
        {
            return armorItem;
        }

        protected set
        {
            #region old
            //if (armorItem != value && armorItem != null)
            //{
            //    if (armorItem.itemEffectOnPlayer != null)
            //    {
            //        GameObject currentItemEffect = itemEffectParent.Find(armorItem.itemEffectOnPlayer.name).gameObject;
            //        if (currentItemEffect != null)
            //        {
            //            currentItemEffect.SetActive(false);
            //        }

            //    }

            //    if (armorSpriteIcondefault != null && armorSpriteIcondefault != null)
            //    {
            //        armorIconSpriteRenderer.sprite = armorSpriteIcondefault;
            //    }

            //    armorItem.OnItemDropped();

            //    armorItem.gameObject.SetActive(true);
            //    if (itemDroppingTransform != null)
            //    {
            //        itemDroppingTransform.localPosition = new Vector3(Mathf.Abs(itemDroppingTransform.localPosition.x) * controller.Direction, itemDroppingTransform.localPosition.y, itemDroppingTransform.localPosition.z);
            //        armorItem.transform.position = itemDroppingTransform.position;
            //    }
            //    else
            //    {
            //        armorItem.transform.position = this.transform.position;
            //    }
            //    LeanTween.cancel(armorItem.gameObject);
            //    armorItem.transform.LeanMove(new Vector3(armorItem.transform.position.x, this.transform.position.y, armorItem.transform.position.z), 0.5f);
            //}
            #endregion
            CheckAndRemoveOldItem(armorItem, value, armorIconSpriteRenderer, armorSpriteIcondefault);
            armorItem = value;
            SubmitNewItem(armorItem, armorIconSpriteRenderer);
            #region old
            //if (armorItem.itemEffectOnPlayer != null)
            //{
            //    GameObject itemEffect = itemEffectParent.Find(armorItem.itemEffectOnPlayer.name).gameObject;
            //    if (itemEffect != null)
            //    {
            //        itemEffect.SetActive(true);
            //    }
            //    else
            //    {
            //        itemEffect = GameObject.Instantiate(armorItem.itemEffectOnPlayer, itemEffectParent);
            //        itemEffect.name = string.IsNullOrEmpty(armorItem.name) ? armorItem.gameObject.name : armorItem.name;
            //        itemEffect.transform.localPosition = Vector3.zero;
            //    }

            //}
            //if (armorItem.itemIcon != null && armorIconSpriteRenderer != null)
            //{
            //    armorIconSpriteRenderer.sprite = armorItem.itemIcon;
            //}
            //LeanTween.cancel(armorItem.gameObject);
            //armorItem.transform.LeanMove(this.transform.position, 0.5f).setOnComplete(() => { armorItem.gameObject.SetActive(false); armorItem.OnItemPick(); });
            #endregion
        }
    }

    public Item PowerItem
    {
        get
        {
            return powerItem;
        }

        protected set
        {
            CheckAndRemoveOldItem(powerItem, value, powerIconSpriteRenderer, powerSpriteIcondefault);
            powerItem = value;
            SubmitNewItem(powerItem, powerIconSpriteRenderer);
        }
    }
    #region User Methods

    private void SubmitNewItem(Item newItem, Image imageRenderer)
    {
        if (newItem.itemEffectOnPlayer != null)
        {
            Transform itemEffect = itemEffectParent.Find(newItem.itemEffectOnPlayer.name);
            if (itemEffect != null)
            {
                itemEffect.gameObject.SetActive(true);
            }
            else
            {
                itemEffect = GameObject.Instantiate(newItem.itemEffectOnPlayer, itemEffectParent).transform;
                itemEffect.gameObject.name = string.IsNullOrEmpty(newItem.name) ? newItem.gameObject.name : newItem.name;
                itemEffect.localPosition = Vector3.zero;
            }

        }
        if (newItem.itemIcon != null && imageRenderer != null)
        {
            imageRenderer.sprite = newItem.itemIcon;
        }
        LeanTween.cancel(newItem.gameObject);
        newItem.transform.LeanMove(this.transform.position, 0.2f).setOnComplete(() => { newItem.gameObject.SetActive(false); newItem.OnItemPick(); });
        
    }
    private void CheckAndRemoveOldItem(Item oldItem, Item newItem, Image imageRenderer, Sprite defaultIcon)
    {
        if (oldItem != newItem && oldItem != null)
        {
            if (oldItem.itemEffectOnPlayer != null)
            {
                Transform currentItemEffect = itemEffectParent.Find(oldItem.itemEffectOnPlayer.name);
                if (currentItemEffect != null)
                {
                    currentItemEffect.gameObject.SetActive(false);
                }

            }

            if (imageRenderer != null && defaultIcon != null)
            {
                imageRenderer.sprite = defaultIcon;
            }

            oldItem.OnItemDropped();
            oldItem.transform.parent = currentRoom.transform;
            oldItem.gameObject.SetActive(true);
            if (itemDroppingTransform != null)
            {
               // itemDroppingTransform.localPosition = new Vector3(itemDroppingTransform.localPosition.x) * controller.Direction, itemDroppingTransform.localPosition.y, itemDroppingTransform.localPosition.z);
                oldItem.transform.position = itemDroppingTransform.position;
            }
            else
            {
                oldItem.transform.position = this.transform.position;
            }
            LeanTween.cancel(oldItem.gameObject);

            //new
            RaycastHit2D hit = Physics2D.Raycast(oldItem.transform.position, Vector2.down, 100,layerMask: groundLayers);
            if (hit)
            {
                oldItem.transform.LeanMove(hit.point + new Vector2(0,0.5f), 0.5f);
            }


            //end new
            //oldItem.transform.LeanMove(new Vector3(oldItem.transform.position.x, this.transform.position.y, oldItem.transform.position.z), 0.5f);
        }
    }
    public void OnPlayerHit(float damage)
    {

    }

    public void PickItem(Item itemPicked)
    {
        switch (itemPicked.type)
        {
            case ItemType.Weapon:
                WeaponItem = itemPicked;
                break;
            case ItemType.Armor:
                ArmorItem = itemPicked;
                break;
            case ItemType.Power:
                PowerItem = itemPicked;
                break;
        }
    }

    public void ApplyArmorStats(ArmorStats armor)
    {
        if(armor != null)
        {
            maxHp.Value = armor.maxHp;
            controller.MoveSpeed.Value = armor.moveSpeed;
            sheildBlockTicks.Value = armor.sheildBlockTicks;
        }
        else
        {
            if (defaultValues != null)
            {
                ApplyArmorStats(defaultValues.DefaultArmorStats);
            }
        }
    }

    #endregion

    #region MonoBehaviours
    private void Awake()
    {
        if (instance == null) instance = this;


        if (weaponIconSpriteRenderer != null) weaponSpriteIcondefault = weaponIconSpriteRenderer.sprite;
        if (armorIconSpriteRenderer != null) armorSpriteIcondefault = armorIconSpriteRenderer.sprite;
        if (powerIconSpriteRenderer != null) powerSpriteIcondefault = powerIconSpriteRenderer.sprite;

        if(defaultValues != null)
        {
            GameObject sword = GameObject.Instantiate(defaultValues.swordDefault.gameObject);
            GameObject armor = GameObject.Instantiate(defaultValues.armorDefault.gameObject);
            GameObject power = GameObject.Instantiate(defaultValues.powerDefault.gameObject);

            PickItem(sword.GetComponent<Item>());
            PickItem(armor.GetComponent<Item>());
            PickItem(power.GetComponent<Item>());

        }
        

        controller = this.GetComponent<CharacterController2D>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("UseItemPower"))
        {
            Debug.Log("F or R");

            if (PowerItem != null)
            {
                PowerItem.UseItem();
            }
        }
    }
    #endregion

}
