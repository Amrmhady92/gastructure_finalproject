﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "KandooZ/DefaultValues")]
public class DefaultValues : ScriptableObject
{

    public Item swordDefault;
    public Item armorDefault;
    public Item powerDefault;

    [SerializeField]
    private WeaponStats defaultWeaponStats = new WeaponStats()
    {
        attackDamage = 1,
        attackDamageModifier = 2,
        attackSpeed = 1,
        attackType = AttackType.Slash,
        attackDistance = 1.65f,
        canCombo2 = true,
        canCombo3 = true,
        debuffType = EnemyDebuffs.None
    };

    [SerializeField]
    private ArmorStats defaultArmorStats = new ArmorStats()
    {
        maxHp = 5,
        moveSpeed = 7,
        sheildBlockTicks = 0
    };

    public WeaponStats DefaultWeaponStats
    {
        get
        {
            return defaultWeaponStats;
        }
    }

    public ArmorStats DefaultArmorStats
    {
        get
        {
            return defaultArmorStats;
        }
    }
}
