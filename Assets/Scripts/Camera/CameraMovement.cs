﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float lerpSpeed = 0.01f;
    public GameObject follower;
    void Start()
    {

    }

    void Update()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, follower.transform.position, lerpSpeed * Time.deltaTime);
    }
}
