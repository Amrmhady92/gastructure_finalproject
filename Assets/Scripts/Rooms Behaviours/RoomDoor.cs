﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDoor : Interactable
{
    public int nextRoomId = 0;
    public bool previousRoomDoor = false;
    public Room currentRoom;
    public Transform roomDoorStartPosition;

    public bool startDoor = false;

    private void Start()
    {
        //Check room next
        
        if (!startDoor && !previousRoomDoor)
        {
            Room nextRoom = currentRoom.GetRoomExitsRoom(nextRoomId);
            if (nextRoom == null)
            {
                Debug.Log("Door " + this.gameObject.name + " in room " + currentRoom.gameObject.name + ", Regestered no Next Room");
                this.gameObject.SetActive(false);
            }
        }
    }

    public override void Interact(GameObject interactingObject)
    {
        
        if (currentRoom != null)
        {
            Room nextRoom = currentRoom;
            if (previousRoomDoor)
            {
                Transform nextTransformTarget = null;
                //Debug.Log("PreviousDoor");
                for (int i = 0; i < currentRoom.previousRoom.roomDoors.Count; i++)
                {
                    if (currentRoom.previousRoom.roomDoors[i].previousRoomDoor) continue;

                    //Debug.Log("Testing doors");
                    nextRoom = currentRoom.previousRoom.roomDoors[i].currentRoom.GetRoomExitsRoom(currentRoom.previousRoom.roomDoors[i].nextRoomId);
                    Debug.Log(nextRoom);
                    if (nextRoom/*.id*/ == currentRoom/*.id*/)
                    {
                        //Debug.Log("Found");
                        nextTransformTarget = currentRoom.previousRoom.roomDoors[i].roomDoorStartPosition;
                        break;
                    }
                }

                // Transform nextTransformTarget = currentRoom.previousRoom.roomDoors.Find(x => x.currentRoom.GetRoomExitsRoom(nextRoomId) == currentRoom).roomDoorStartPosition;
                if (nextTransformTarget != null)
                {
                    currentRoom.gameObject.SetActive(false);
                    currentRoom.previousRoom.gameObject.SetActive(true);

                    //Sets Player Position In next room
                    CharacterActions.Instance.gameObject.transform.position = nextTransformTarget.position;
                    //Sets Player Current Room
                    CharacterStats.Instance.currentRoom = currentRoom.previousRoom;
                }
            }
            else
            {
                
                nextRoom = currentRoom.GetRoomExitsRoom(nextRoomId);
                if (nextRoom != null && nextRoom.startPositionTransform != null)
                {
                    currentRoom.gameObject.SetActive(false);
                    nextRoom.gameObject.SetActive(true);

                    //Sets Player Current Room
                    CharacterStats.Instance.currentRoom = nextRoom;
                    //Sets Player Position In next room
                    CharacterActions.Instance.gameObject.transform.position = nextRoom.startPositionTransform.position;
                }

                
            }
        }

    }
}
