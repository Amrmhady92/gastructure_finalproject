﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    NoExit,
    OneExit,
    TwoExit,
    ThreeExit,
    Shop,
    Item,
    Boss
}

[System.Serializable]
public class Stage
{
    public string name;
    public int noExitRooms = 0;
    public int oneExitRooms = 0;
    public int twoExitRooms = 0;
    public int threeExitRooms = 0;
    public int shopRooms = 0;
    public int itemRooms = 0;
    public int bossRooms = 0;

    public List<Room> stageRooms;


}
public class RoomsGenerator : MonoBehaviour
{
    public List<Stage> stages;
    public int areaNumber = 1;
    public Room startRoom;
    public RoomDoor areaRoomDoor;
    // 1 -> 1
    // 2 -> 2
    // 3 -> 4
    // 4 -> 8
    // 5 -> 1


    public RoomsData data;


    private GameObject parentGO;
    Room tmpRoom;
    int id = 0;

    private void Start()
    {
        GenerateRooms();
    }
    private void GenerateRooms()
    {
        parentGO = new GameObject("Rooms " + areaRoomDoor.nextRoomId);
        id = 0;
        // Each Stage has all its rooms in the list.
        for (int i = 0; i < stages.Count; i++)
        {

            stages[i].stageRooms = new List<Room>();

            CreateStageRooms(i, stages[i].noExitRooms, RoomType.NoExit);

            CreateStageRooms(i, stages[i].oneExitRooms, RoomType.OneExit);
            //for (int j = 0; j < stages[i].oneExitRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.OneExit);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            CreateStageRooms(i, stages[i].twoExitRooms, RoomType.TwoExit);
            //for (int j = 0; j < stages[i].twoExitRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.TwoExit);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            CreateStageRooms(i, stages[i].threeExitRooms, RoomType.ThreeExit);
            //for (int j = 0; j < stages[i].threeExitRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.ThreeExit);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            CreateStageRooms(i, stages[i].shopRooms, RoomType.Shop);
            //for (int j = 0; j < stages[i].shopRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.Shop);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            CreateStageRooms(i, stages[i].itemRooms, RoomType.Item);
            //for (int j = 0; j < stages[i].itemRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.Item);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            CreateStageRooms(i, stages[i].bossRooms, RoomType.Boss);
            //for (int j = 0; j < stages[i].bossRooms; j++)
            //{
            //    tmpRoom = data.GetRoom(RoomType.Boss);
            //    if (tmpRoom != null)
            //    {
            //        CreateRoom(i);
            //    }
            //}

            stages[i].stageRooms.Shuffle();
        }

        List<Room> nextStageRoomsList = null;
        //Assigning Next Rooms to the rooms of each stage
        for (int i = 0; i < stages.Count; i++)
        {
            //Stage , Check all rooms

            int rooms = 0;
            if(i + 1 < stages.Count) nextStageRoomsList = new List<Room>(stages[i + 1].stageRooms);

            #region Testing
            ////Testing

            //int testrommcount = 0;
            //for (int g = 0; g < stages[i].stageRooms.Count; g++)
            //{
            //    switch (stages[i].stageRooms[g].roomType)
            //    {
            //        case RoomType.NoExit:
            //            testrommcount += 0;
            //            break;
            //        case RoomType.OneExit:
            //            testrommcount += 1;
            //            break;
            //        case RoomType.TwoExit:
            //            testrommcount += 2;
            //            break;
            //        case RoomType.ThreeExit:
            //            testrommcount += 3;
            //            break;
            //    }
            //}
            //Debug.Log(" in Stage number "+i+" , nextStageRoomsList count = " + nextStageRoomsList.Count + "  and tested rooms = "+testrommcount);

            ////End testing
            #endregion

            for (int j = 0; j < stages[i].stageRooms.Count; j++) // Check Stages all rooms
            {
                switch (stages[i].stageRooms[j].roomType)
                {
                    case RoomType.NoExit:
                        rooms = 0;
                        break;
                    case RoomType.OneExit:
                        rooms = 1;
                        break;
                    case RoomType.TwoExit:
                        rooms = 2;
                        break;
                    case RoomType.ThreeExit:
                        rooms = 3;
                        break;
                }

                if ((i + 1) < stages.Count) //Not last Stage
                {
                    stages[i].stageRooms[j].roomExits = new List<Room>();
                    for (int x = 0; x < rooms; x++)
                    {
                        //if(nextStageRoomsList.Count > 0)
                        //{
                            stages[i].stageRooms[j].roomExits.Add(nextStageRoomsList[0]);
                            nextStageRoomsList[0].previousRoom = stages[i].stageRooms[j];

                            nextStageRoomsList.RemoveAt(0);
                       // }
                        
                    }
                    rooms = 0;
                }
                else // Last Stage
                {

                }

                if(i == 0)
                {
                    for (int x = 0; x < stages[i].stageRooms.Count; x++)
                    {
                        stages[i].stageRooms[x].previousRoom = startRoom;
                        if (startRoom.roomExits == null) startRoom.roomExits = new List<Room>();
                        startRoom.roomExits.Add(stages[i].stageRooms[x]);
                    }
                }

                stages[i].stageRooms[j].SetRoomVariables();
            }
        }
    }

    private void CreateStageRooms(int i, int numberOfRoom, RoomType roomType)
    {
        for (int j = 0; j < numberOfRoom; j++)
        {
            tmpRoom = data.GetRoom(roomType);
            if (tmpRoom != null)
            {
                CreateRoom(i);
            }
        }
    }

    private void CreateRoom(int stageNumber)
    {
        tmpRoom = GameObject.Instantiate(tmpRoom, parentGO.transform);
        tmpRoom.gameObject.SetActive(false);
        tmpRoom.id = id;
        id++;
        tmpRoom.stage = stageNumber + 1;
        tmpRoom.area = areaNumber;
        tmpRoom.name = "Stage : " + tmpRoom.stage + " , Room : " + (tmpRoom.id);
        stages[stageNumber].stageRooms.Add(tmpRoom);
    }
}
