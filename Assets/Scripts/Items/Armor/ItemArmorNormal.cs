﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArmorStats
{
    public float maxHp = 5;
    public float moveSpeed = 1;
    public int sheildBlockTicks = 0;
}

public class ItemArmorNormal : Item
{

    public ArmorStats armorStats;

    public override void OnItemDropped()
    {
        CharacterStats.Instance.ApplyArmorStats(null);
    }

    public override void OnItemPick()
    {
        CharacterStats.Instance.ApplyArmorStats(armorStats);
    }


}
