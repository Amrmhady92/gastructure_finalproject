﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public RoomType roomType;
    public List<Room> roomExits;
    public List<RoomDoor> roomDoors;

    public Transform startPositionTransform;
    public int stage;
    public int area;
    public int id = 0;
    public Room previousRoom;
    public ItemsList itemsList;
    //public int specificItem = -1;

    private static List<int> specificItemsList;

    //public InteractableBuy[] buyingOptions;


    private readonly int maxExits;

    public int MaxExits
    {
        get
        {
            switch (roomType)
            {
                case RoomType.NoExit:
                    return 0;
                case RoomType.OneExit:
                    return 1;
                case RoomType.TwoExit:
                    return 2;
                case RoomType.ThreeExit:
                    return 3;
                case RoomType.Shop:
                case RoomType.Item:
                    return 0;
            }
            return 0;
        }
    }

    public static List<int> SpecificItemsList
    {
        get
        {
            if(specificItemsList == null || specificItemsList.Count == 0)
            {
                specificItemsList = new List<int>() { 0, 1, 2, -1 };
                specificItemsList.Shuffle();
            }
            //Debug.Log("before: " + specificItemsList.Count);
            //int item = specificItemsList.GetRandomValue(true);
            //Debug.Log("after : " + specificItemsList.Count);

            //specificItemsList.Remove(item);
            return specificItemsList;
        }
    }



    public void OnEnable()
    {
        //Testing
        if(this.GetComponentInChildren<TMPro.TextMeshProUGUI>() != null) this.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = this.gameObject.name;
    }
    public Room GetRoomExitsRoom(int roomIndex)
    {

        if (roomIndex < roomExits.Count && roomIndex >= 0)
        {
            return roomExits[roomIndex];
        }
        else if(roomIndex == -1)
        {
            return previousRoom;
        }
        else
        {
            Debug.LogError("Wrong Exit room Index");
            return null;
        }
    }

    internal void SetRoomVariables()
    {
        switch (roomType)
        {
            case RoomType.NoExit:
            case RoomType.OneExit:
            case RoomType.TwoExit:
            case RoomType.ThreeExit:

                //Set Enemy Types

                break;
            case RoomType.Shop:
                //Set Shop Items Quality
                int[] itemSpecificRandoming = area == 1 ? new int[] { 0, 0, 1, 1 } : new int[] { 0, 0, 1, 1, 2, 2 };
                itemSpecificRandoming.Shuffle();

                InteractableBuy[] buyingOptions = this.gameObject.GetComponentsInChildren<InteractableBuy>();

                if (buyingOptions.Length > itemSpecificRandoming.Length) Debug.Log("problem");
                for (int i = 0; i < buyingOptions.Length; i++)
                {
                    buyingOptions[i].Set(area, itemSpecificRandoming[i]);
                }
                break;

            case RoomType.Item:

                GameObject itemHolder = transform.Find("ItemHolder").gameObject;
                int tier = 1;
                if(stage < 2)
                {
                    tier = area;
                }
                else
                {
                    tier = Mathf.Min( area + 1, 5); // 5 is max tier
                }


                int itemspec = SpecificItemsList.GetRandomValue(true);

                Item item = itemsList.GetRandomItem(tier, itemspec);
                if(item != null)
                {
                    if(itemHolder != null)
                    {
                        item = GameObject.Instantiate(item, itemHolder.transform);
                        item.transform.position = itemHolder.transform.position;
                    }
                    else
                    {
                        Debug.Log("No item Holder");
                    }
                }
                else
                {
                    Debug.Log("Couldnt Find item");
                }

                //Set Item Quality

                break;
            case RoomType.Boss:

                //Set Boss
                //Dropped Item = Area +1 or 50% to be area+2;
                break;

        }
    }



}
