﻿using System;
using System.Collections.Generic;
using UnityEngine;
using KandooZ;

public class CharacterAnimationController : MonoBehaviour
{
    public CharacterController2D moveController;
    public CharacterCombatController combatController;
    private Animator animator;
    public FloatField attackSpeedModifier;
    private float animatorStartSpeed = 1;

    private void Awake()
    {
        animator = this.GetComponent<Animator>();
        if (moveController == null) moveController = this.GetComponentInParent<CharacterController2D>();
        if (combatController == null) combatController = this.GetComponentInParent<CharacterCombatController>();
        animatorStartSpeed = animator.speed;
    }
    private void Update()
    {
        animator.SetBool("running", moveController.Running);
        animator.SetBool("attack01Bool", combatController.Attack01);
        animator.SetBool("attack02Bool", combatController.Attack02);
        animator.SetBool("attack03Bool", combatController.Attack03);
        animator.SetBool("grounded", moveController.Grounded);

        if (combatController.Attack01 || combatController.Attack02 || combatController.Attack03)
        {
            animator.speed = attackSpeedModifier.Value;
        }
    }


    //Called at the end of each attack animation through animation events
    private void ResetBool(int boolIndex)
    {
        switch (boolIndex)
        {
            case 1:
                
                combatController.Attack01 = false;
                if (combatController.Attack02 == false)
                {
                    moveController.Active = true;
                    animator.speed = animatorStartSpeed;
                }

                if (moveController.Running)
                {
                    combatController.Attack02 = false;
                    combatController.Attack03 = false;
                    animator.SetBool("attack01Bool",false);
                    animator.SetBool("attack02Bool",false);
                }
                break;
            case 2:
                combatController.Attack01 = false;
                combatController.Attack02 = false;
                if (combatController.Attack03 == false)
                {
                    moveController.Active = true;
                    animator.speed = animatorStartSpeed;
                }
                if (moveController.Running)
                {
                    combatController.Attack03 = false;
                    animator.SetBool("attack03Bool", false);
                }
                break;
            case 3:
                combatController.Attack01 = false;
                combatController.Attack02 = false;
                combatController.Attack03 = false;
                moveController.Active = true;
                animator.speed = animatorStartSpeed;
                break;
        }
    }

    public void CheckAttack(int attackNumber)
    {
        combatController.OnAttack(/*onAttack.Invoke(*/attackNumber);
    }
}
