﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KandooZ
{
    [CreateAssetMenu(menuName = "KandooZ/Fields/Vector3Field")]
    public class Vector3Field : ScriptableObject
    {

        [SerializeField]
        public  Vector3 value;


        //public static implicit operator Vector3(Vector3Field b)
        //{
        //    return b.value;
        //}
    }
}