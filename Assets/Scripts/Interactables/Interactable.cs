﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Interactable : MonoBehaviour
{
    public GameObject interactImage;
    public bool interactable = true;
    public abstract void Interact(GameObject interactingObject = null);
    public virtual void SetInteractableImage(bool onOff = true)
    {
        if (interactable || !onOff)
        {
            if (interactImage != null) interactImage.gameObject.SetActive(onOff);
        }
    }

    private bool shaking = false;
    public virtual void ShakeButton()
    {
        if (shaking || interactImage == null) return;
        shaking = true;
        Vector3 startPos = interactImage.transform.position;
        LeanTween.move(interactImage, startPos + new Vector3(0.2f, 0, 0), 0.1f).setEaseOutElastic().setOnComplete(() =>
        {
            LeanTween.move(interactImage, startPos - new Vector3(0.2f, 0, 0), 0.1f).setEaseOutElastic().setOnComplete(() =>
            {
                LeanTween.move(interactImage, startPos, 0.1f).setEaseOutElastic().setOnComplete(() =>
                {
                    shaking = false;
                });
            });
        });
    }

    private bool clicking = false;

    public virtual void ButtonClicked()
    {
        if (clicking || interactImage == null) return;
        clicking = true;
        Vector3 startScale = interactImage.transform.localScale;
        LeanTween.scale(interactImage, startScale - new Vector3(0.1f, 0.1f, 0.1f), 0.1f).setEaseOutElastic().setOnComplete(() =>
        {
            LeanTween.scale(interactImage, startScale + new Vector3(0.1f, 0.1f, 0.1f), 0.1f).setEaseOutElastic().setOnComplete(() =>
            {
                LeanTween.scale(interactImage, startScale, 0.1f).setEaseOutElastic().setOnComplete(() =>
                {
                    clicking = false;
                    SetInteractableImage(false);
                });
            });
        });
    }




}

