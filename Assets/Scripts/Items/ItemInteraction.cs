﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInteraction : Interactable
{
    
    private void Awake()
    {
        
    }
    public override void Interact(GameObject interactingObject)
    {
        Item item = this.GetComponent<Item>();

        CharacterStats player = interactingObject.GetComponent<CharacterStats>();
        if (player != null && item != null)
        {
            player.PickItem(item);
        }
        else
        {
            if(item == null)
            {
                Debug.LogError("No Item on Item Interaction");
            }
            if(player == null)
            {
                Debug.LogError("Didnt find player on interacting object");
            }
        }
    }
}
