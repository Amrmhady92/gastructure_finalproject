﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KandooZ
{
    [CreateAssetMenu(menuName = "KandooZ/Fields/IntField")]
    [System.Serializable]
    public class IntField : ScriptableObject
    {

        [SerializeField]
        private int value;

        public event System.Action<int> OnValueChanged;
        public int Value
        {
            get
            {
                return value;
            }

            set
            {
                if (Mathf.Abs(value - this.value) > 0)
                {
                    OnValueChanged?.Invoke(value);
                    this.value = value;
                }
            }
        }

        public string StringValue
        {
            get
            {
                return value.ToString();
            }

            set
            {
                if (Mathf.Abs(int.Parse(value) - this.value) > 0)
                {
                    OnValueChanged?.Invoke(int.Parse(value));
                    this.value = int.Parse(value);
                }
            }
        }

        public float FloatValue
        {
            get
            {
                return value;
            }

            set
            {
                //if (Mathf.Abs(value - this.value) > 0)
                //{
                    //OnValueChanged?.Invoke(value);
                    this.value = (int)value;
                //}
            }
        }

        //public static implicit operator int(IntField b)
        //{
        //    return b.value;
        //}
        //public static implicit operator IntField(int b)
        //{
        //    return b;
        //}

    }
}