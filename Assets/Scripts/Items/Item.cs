﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Weapon,
    Armor,
    Power
}
public abstract class Item : MonoBehaviour
{
    public ItemType type;
    public int tier = 1;
    public new string name;
    public int itemPrice = 0;
    public Sprite itemIcon;
    public GameObject itemEffectOnPlayer;

    public abstract void OnItemPick();
    public abstract void OnItemDropped();
    public virtual void UseItem()
    {
        Debug.Log("override to use");
    }


}
