﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySimple : EnemyBehaviour
{
    public float knockbackDistance = 0.2f;
    public float jumpUpDistance = 0.1f;
    public float damageToPlayer = 1;
    protected override void OnDeath()
    {
        hitable = false;
        SpriteRenderer spRend = this.GetComponentInChildren<SpriteRenderer>();
        Debug.Log(spRend);
        //if (spRend == null) spRend = this.GetComponent<SpriteRenderer>();
        if(spRend != null)
        {
            
            Color spriteColor = spRend.color;
            StartCoroutine(DoAfter(() => { spRend.color = Color.red; }, 0.1f));
            StartCoroutine(DoAfter(() => { spRend.color = spriteColor; }, 0.2f));
            StartCoroutine(DoAfter(() => { spRend.color = Color.red; }, 0.3f));
            StartCoroutine(DoAfter(() => { spRend.color = spriteColor; }, 0.4f));
            StartCoroutine(DoAfter(() => { this.gameObject.SetActive(false); }, 0.5f));
        }
        //else
        //{
        //    this.gameObject.SetActive(false);
        //}
    }

    protected override void OnHit()
    {
        if (currentDebuff != EnemyDebuffs.None) return;

        Vector3 pos = this.transform.position;
        float knock = hitSide * knockbackDistance;
        hitSide = 0;
        this.transform.Translate(new Vector3(knockbackDistance, 0, 0));
        StopAllCoroutines();
        this.transform.Translate(new Vector3(0, jumpUpDistance,0));
        StartCoroutine(DoAfter(() => { this.transform.position = pos; }, 0.1f));
    }

    protected override void OnHitPlayer()
    {

    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<CharacterStats>() != null)
        {
            CharacterStats.Instance.OnPlayerHit(damageToPlayer);
        }
    }

    private IEnumerator DoAfter(System.Action action, float time = 0.1f)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }

    protected override void ApplyDebuff(EnemyDebuffs debuff)
    {

    }


}
