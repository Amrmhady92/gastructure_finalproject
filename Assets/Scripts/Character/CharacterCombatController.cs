﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KandooZ;

[System.Serializable]
public class WeaponStats
{
    public float attackDamage = 1;
    public float attackDamageModifier = 2;
    public float attackSpeed = 1;
    public float attackDistance = 0.3f;
    public bool canCombo2 = true;
    public bool canCombo3 = true;
    public  EnemyDebuffs debuffType = EnemyDebuffs.None;
    public  AttackType attackType = AttackType.Slash;
}
public enum AttackType
{
    Fire,
    Water,
    Air,
    Earth,
    Pierce,
    Slash,
    Bludgeon
}

public class CharacterCombatController : MonoBehaviour
{

    [Header("Weapon Stats Fields")]
    public FloatField attackDistance;
    public FloatField attackDamage;
    public FloatField attackDamageModifier;
    public FloatField attackSpeed;
    [SerializeField] private EnemyDebuffs attackModifier;
    [SerializeField] private AttackType attackType;
    [Space(10)]
    public LayerMask enemiesLayers;
    [Space(10)]
    public DefaultValues defaultValuesFile;

    public System.Action<int> onAttack;

    private bool canCombo2 = true;
    private bool canCombo3 = true;

    public Transform hittingPointTransform;

    private bool attack01;
    private bool attack02;
    private bool attack03;
    public bool Attack01
    {
        get
        {
            return attack01;
        }

        set
        {
            attack01 = value;
        }
    }
    public bool Attack02
    {
        get
        {
            return attack02;
        }

        set
        {
            attack02 = value;
        }
    }
    public bool Attack03
    {
        get
        {
            return attack03;
        }

        set
        {
            attack03 = value;
        }
    }


    private CharacterController2D moveController;
    private RaycastHit2D[] hits;

    public static CharacterCombatController Instance
    {
        get
        {
            return instance;
        }
    }

    public AttackType AttackType
    {
        get
        {
            return attackType;
        }

        private set
        {
            attackType = value;
        }
    }
    public EnemyDebuffs AttackModifier
    {
        get
        {
            return attackModifier;
        }

        private set
        {
            attackModifier = value;
        }
    }

    private static CharacterCombatController instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        moveController = this.GetComponent<CharacterController2D>();
        onAttack += OnAttack;
    }
    private void Update()
    {
        if (moveController.Grounded)
        {
            if (Input.GetButtonDown("Fire1") && !Attack01)
            {
                Attack01 = true;
                moveController.Active = false;
            }
            else if (Input.GetButtonDown("Fire1") && !Attack02 && canCombo2)
            {
                Attack02 = true;
                moveController.Active = false;
            }
            else if (Input.GetButtonDown("Fire1") && Attack02 && canCombo3)
            {
                Attack03 = true;
                moveController.Active = false;
            }
        }
    }

    private float dealtDamage = 0;
    public void OnAttack(int attackNumber)
    {
        hits = Physics2D.RaycastAll(hittingPointTransform.position, Vector2.right * moveController.Direction, attackDistance.Value, enemiesLayers);

        if(hits.Length > 0)
        {
            switch (attackNumber)
            {
                case 0:
                case 1:
                    dealtDamage = attackDamage.Value;
                    break;
                case 2:
                    dealtDamage = attackDamage.Value + attackDamageModifier.Value;
                    break;
                default:
                    dealtDamage = attackDamage.Value;
                    break;
            }
        }

        for (int i = 0; i < hits.Length; i++)
        {
            EnemyBehaviour enemy = hits[i].collider.gameObject.GetComponent<EnemyBehaviour>();
            if (enemy != null)
            {
                enemy.HitRecieved(dealtDamage, hits[i].point, AttackModifier);
            }
        }
    }


    public void SetWeaponStats(WeaponStats newStats)
    {
        if(newStats != null)
        {
            attackDamage.Value = newStats.attackDamage;
            attackDamageModifier.Value = newStats.attackDamageModifier;
            attackSpeed.Value = newStats.attackSpeed;
            AttackType = newStats.attackType;
            AttackModifier = newStats.debuffType;
            canCombo2 = newStats.canCombo2;
            canCombo3 = newStats.canCombo3;
            attackDistance.Value = newStats.attackDistance;
        }
        else
        {
            if (defaultValuesFile != null)
            {
                SetWeaponStats(defaultValuesFile.DefaultWeaponStats);
            }
        }
    }
    //private void OnDrawGizmos()
    //{
    //    if(hittingPointTransform != null)
    //    {
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawLine(hittingPointTransform.position, hittingPointTransform.position + (Vector3.right * moveController.Direction * attackDistance));
    //        //Ray ray = new Ray(hittingPointTransform.position, Vector3.right * moveController.Direction * attackDistance);
    //        //Gizmos.DrawRay(ray);
    //    }
       
    //}
}
